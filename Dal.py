"""
@FILE_NAME : Dal
-*- coding : utf-8 -*-
@Author : Zhaokugua
@Time : 2021/12/28 9:54
"""
import requests
normal_headers = {'User-Agent': 'bilibili Security Browser Edg/87.0.4280.88'}


# 可用于下载歌词、专辑封面、获取简介作为文件名等
def get_info_by_auid(auid):
    """
    通过auid获取歌曲简介信息
    包括标题、封面、歌词、时间、up主、音频清晰度列表等
    :param auid:歌曲的au号
    :return json:
    """
    res_json = requests.get(f'http://api.bilibili.com/audio/music-service-c/songs/playing?song_id={auid}', headers=normal_headers).json()
    return res_json


# 用于快速下载（网页版请求，仅支持192k）
def get_fast_down_info(auid):
    """
    【快速下载模式】
    通过auid获取快速下载api的下载链接等信息
    webapi 仅支持192k音质
    :param auid:歌曲的au号
    :return json:
    """
    '''
    注意：快速下载api需要使用至少以下header才能正常返回（状态码206），否则会403
    headers = {
        'range': 'bytes=0-',
        'referer': 'https://www.bilibili.com/',
        'User-Agent': 'bilibili Security Browser Edg/87.0.4280.88'
    }
    '''
    res_json = requests.get(f'http://www.bilibili.com/audio/music-service-c/web/url?sid={auid}').json()
    return res_json


# 用于一般下载
def get_down_info(auid, quality=3, SESSDATA='', DedeUserID=''):
    """
    【普通下载模式】
    获取下载信息（包含下载链接、大小、音质列表、标题、封面等
    quality 0,1,2,3 分别对应128k 192k 320k和flac无损音质
    音质不存在时将自动降低音质，最终音质以cdn链接中的-xxxk为准
    无损音质解析需要大会员，即可选参数SESSDATA和DedeUserID用于登录
    :param quality:音频质量参数
    :param auid:歌曲的au号
    :param DedeUserID:b站登录cookie（用户的uid）
    :param SESSDATA:b站登录cookie
    :return json:
    """
    url = f'http://api.bilibili.com/audio/music-service-c/url?build=6140500&mid=17786092&platform=android&privilege=2' \
          f'&quality={quality}&songid={auid} '
    headers = {
        'referer': 'https://www.bilibili.com/',
        'User-Agent': 'bilibili Security Browser Edg/87.0.4280.88',
        'cookie': f'DedeUserID={DedeUserID}; SESSDATA={SESSDATA}' if DedeUserID and SESSDATA else ''
    }
    res_info = requests.get(url, headers=headers).json()
    return res_info


# 用于获取歌单列表便于批量下载
def get_info_by_amid(amid):
    """
    通过amid获取歌单信息
    :param amid: 歌单的am号
    :return json:
    """
    res_info = requests.get(f'http://api.bilibili.com/audio/music-service-c/menus/{amid}', headers=normal_headers).json()
    return res_info


# 用于获取自己的歌单列表
def get_collection_info(SESSDATA, DedeUserID, page_index=0):
    """
    获取歌单信息（适用于自己创建的歌单）
    需要登录
    :param page_index: 当前页数（从0开始）
    :param DedeUserID:b站登录cookie（用户的uid）
    :param SESSDATA:b站登录cookie
    :return json:
    """
    headers = {
        'referer': 'https://www.bilibili.com/',
        'User-Agent': 'bilibili Security Browser Edg/87.0.4280.88',
        'cookie': f'DedeUserID={DedeUserID}; SESSDATA={SESSDATA}'
    }
    url = f'http://api.bilibili.com/audio/music-service-c/collections?access_key=&actionKey=appkey&appkey' \
          f'=27eb53fc9058f8c3&build=8930&device=phone&mid={DedeUserID}&page_index={page_index}&page_size=3'
    res_info = requests.get(url=url, headers=headers).json()
    return res_info


# 用于获取个人歌单列表详情
def get_info_by_collection_id(collection_id, pn=1):
    """
    通过collection_id获取歌单信息（适用于自己创建的歌单）
    :param pn: page_num 页数（从1开始）
    :param collection_id: 歌单收藏号（sid）
    :return json:
    """
    url = f'https://www.bilibili.com/audio/music-service-c/web/song/of-coll?sid={collection_id}&pn={pn}&ps=100'
    res_info = requests.get(url=url, headers=normal_headers).json()
    return res_info


if __name__ == '__main__':
    info = get_info_by_auid(auid=531500)
    fast_down_info = get_fast_down_info(auid=284550)
    down_info = get_down_info(auid=284550, quality=3)
    am_info = get_info_by_amid(amid=10624)
    # collection_info = get_collection_info()
    collection_info2 = get_info_by_collection_id(collection_id=87099)
    print('喵')
